*start

[cm]
[clearfix]
[chara_config pos_mode=false]

[mask time=0]


[position layer="message0" left=20 top=450 width=920 height=150 page=fore visible=true]

[position layer=message0 page=fore margint="35" marginl="50" marginr="70" marginb="60"]

[ptext name="chara_name_area" layer="message0" color="white" size=22 x=50 y=460]

[chara_config ptext="chara_name_area"]


;あかね
[chara_new  name="akane" storage="chara/akane/normal.png" jname="あかね"]

;あおい
[chara_new  name="aoi" storage="chara/aoi/normal.png" jname="あおい"]

;あかね表情
[chara_face name="akane" face="normal" storage="chara/akane/normal.png"]
[chara_face name="akane" face="angry" storage="chara/akane/angry.png"]
[chara_face name="akane" face="doki" storage="chara/akane/doki.png"]
[chara_face name="akane" face="happy" storage="chara/akane/happy.png"]
[chara_face name="akane" face="sad" storage="chara/akane/sad.png"]
[chara_face name="akane" face="back" storage="chara/akane/back.png"]



;あおい表情

[chara_face name="aoi" face="normal" storage="chara/aoi/normal.png"]
[chara_face name="aoi" face="angry" storage="chara/aoi/angry.png"]
[chara_face name="aoi" face="doki" storage="chara/aoi/doki.png"]
[chara_face name="aoi" face="happy" storage="chara/aoi/happy.png"]
[chara_face name="aoi" face="sad" storage="chara/aoi/sad.png"]


@showmenubutton

;背景
[image layer=0 folder="bgimage" name="gaikan" storage="gaikan_hiru.jpg" visible=true top=0 left=0 width=1200 height=675]

[mask_off time=1000]
[wait time=200]

[chara_hide_all]

@layopt layer=message0 visible=true

[chara_mod name="akane" face="back"]


[chara_mod name="akane" face="back"]

[chara_show name="akane" left=500 top=150]

[chara_show name="aoi" left=0 top=50 time=0 opacity=0]

[anim name="aoi" time=0  opacity=0]
[anim name="aoi" left="+=50" effect="easeInQuint" time=400 opacity=255]
[wait time=800]


#あおい
……………………[p]

@layopt layer=message0 visible=false


[chara_mod name="aoi" face="happy"]

[anim name="aoi" left="+=100" time=1000]

[wa]

[anim name="aoi" top="+=100" time=1000]

[wa]

[wait time=500]

[anim name="aoi" left="+=50" time=100]

[wa]

[anim name="aoi" left="-=50" time=100]

[wa]

[anim name="aoi" left="+=50" time=100]

[wa]

[anim name="aoi" left="-=50" time=100]

@layopt layer=message0 visible=true

#あおい
……つんつん[p]

;飛び上がる

[anim name="akane" top="-=100" time=100]

[wa]

[anim name="akane" top="+=100" time=100]

#あかね
ぎゃ～！！[p]

[chara_mod name="akane" face="angry"]


#あかね
何するのよ！！！　足しびれてたのに～！！！！[p]

[anim name="aoi" left="-=100" time=1000]

[chara_mod name="aoi" face="happy"]

[wa]

#あおい
あはは、ごめんごめん[p]

[chara_mod name="aoi" face="normal"]

#あおい
ねえねえ、これは何？[p]

[chara_mod name="akane" face="normal"]


#あかね
ああ、それね。焼き芋よ[p]

[chara_mod name="aoi" face="happy"]

#あおい
へ～、おいしそうだね！[p]


#あかね
今、焼くから待ってなさい[p]

@layopt layer=message0 visible=false


[chara_hide_all]

;暗転 少し長め

[mask time=500]

[wait time=500]

[mask_off time=500]

[chara_show name="aoi" left=300 top=50]

;キャラAウロウロ

[chara_mod name="aoi" face="angry"]

[anim name="aoi" left="-=250" time=750]

[wa]

[chara_mod name="aoi" face="normal"]

[anim name="aoi" left="+=500" time=1500]

[wa]

[chara_mod name="aoi" face="angry"]

[anim name="aoi" left="-=250" time=750]

[wa]

@layopt layer=message0 visible=true


#あおい
……[p]

[chara_hide_all]

[chara_mod name="aoi" face="angry"]

[chara_show name="akane" left=500 top=50 wait=false]

[chara_show name="aoi" left=50 top=50 wait=false]

#あかね
落ち着かないみたいだけど、どうしたの？[p]

[chara_mod name="aoi" face="normal"]

#あおい
そろそろ焼けたかなって……[p]


#あかね
そうね、もう少し時間が掛かると思うわ[p]

[chara_mod name="aoi" face="sad"]

#あおい
分かった……[p]

@layopt layer=message0 visible=false

;暗転 少し短め

[chara_hide_all]

[mask time=500]

[mask_off time=500]

[chara_show name="akane" left=500 top=50 wait=false]

[chara_show name="aoi" left=50 top=50 wait=false]

@layopt layer=message0 visible=true

#あかね
うん、いい感じ！　はい、どうぞ[p]

[anim name=akane left="-=100" time=500]

[wa]

[wait time=800]

[anim name=akane left="+=100" time=500]

#あかね
それじゃあ[p]

[chara_mod name="akane" face="happy" wait=false]

[chara_mod name="aoi" face="happy" wait=false]

#あおい・あかね
「「いっただきまーす！」」[p]

[wait time=1000]

@layopt layer=message0 visible=false


[mask color=0xFF0000 time=100]

[chara_mod name="akane" face="normal" time=0]

[chara_mod name="aoi" face="angry" time=0]

[mask_off time=100]

[camera x=-130 y=50 zoom=1.7 time=500]


[anim name="aoi" left="-=10" time=100]

[wa]

[anim name="aoi" left="+=20" time=100]

[wa]

[anim name="aoi" left="-=20" time=100]

[wa]

[anim name="aoi" left="-=10" time=100]

[wa]

[anim name="aoi" left="+=20" time=100]

[wa]

[anim name="aoi" left="-=20" time=100]

[wa]

[wait time=100]

[camera zoom=1 x=0 y=0 time=500]

[wa]

[anim name="aoi" left="+=300" time=1000]

[chara_mod name="akane" face="doki"]
[wa]

[anim name=aoi left="+=10" effect="easeOutQuint" time=500]
[anim name=akane left="+=5" effect="easeOutQuint" time=300]

@layopt layer=message0 visible=true

#あおい
……う、うぅ～！！！！！[p]

[wait time=500]

;キャラB 画面から退出

[chara_mod name="akane" face="angry"]

#あかね
……熱いって！？　大変、お水お水！

[anim name=akane left="+=50" time=200 opacity=255]
[wait time=200]
[chara_hide name="akane" time=300 opacity=0]
[wait time=300]

[wa]

[mask]
@layopt layer=message0 visible=false
@hidemenubutton
[chara_hide_all]
[freeimage layer="base"]
[mask_off]
[jump first.ks]



