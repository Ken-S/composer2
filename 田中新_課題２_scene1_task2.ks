
;■条件
;・使用キャラクターは自由
;・背景は何を使用してもOK

;■スクリプトを組む上でのポイント
;・抱き着く際の演出に違和感がないように
;・普通に組むと盛り上がりに欠けるので、どこかしらに演出を

*start

[cm]
[clearfix]
[chara_config pos_mode=false]

[position layer="message0" left=20 top=400 width=920 height=200 page=fore visible=true]

[position layer=message0 page=fore margint="45" marginl="50" marginr="70" marginb="60"]

[ptext name="chara_name_area" layer="message0" color="white" size=24 x=50 y=410]

[chara_config ptext="chara_name_area"]

;————————以下よりスクリプトを組んでください————————

;キャラ１定義
[chara_new name="akane" storage="chara/akane/normal.png" jname="あかね"]

;キャラ２定義
[chara_new name="hi" storage="chara/hi/hi01.png" jname="hi"]

;キャラ１表情
[chara_face name="akane" face="normal" storage="chara/akane/normal.png" jname="あかね"]
[chara_face name="akane" face="happy" storage="chara/akane/happy.png" jname="あかね"]
[chara_face name="akane" face="doki" storage="chara/akane/doki.png" jname="あかね"]
[chara_face name="akane" face="sad" storage="chara/akane/sad.png" jname="あかね"]
[chara_face name="akane" face="angry" storage="chara/akane/angry.png" jname="あかね"]

;キャラ２表情
[chara_face name="hi" face="normal" storage="chara/hi/hi01.png" jname="hi"]
[chara_face name="hi" face="komarimax" storage="chara/hi/hi02.png" jname="hi"]
[chara_face name="hi" face="happy" storage="chara/hi/hi03.png" jname="hi"]
[chara_face name="hi" face="doki" storage="chara/hi/hi04.png" jname="hi"]
[chara_face name="hi" face="sad" storage="chara/hi/hi05.png" jname="hi"]
[chara_face name="hi" face="nicejitome" storage="chara/hi/hi06.png" jname="hi"]
[chara_face name="hi" face="okoraretai" storage="chara/hi/hi07.png" jname="hi"]
[chara_face name="hi" face="nannkaeroi" storage="chara/hi/hi08.png" jname="hi"]

;背景
[bg storage="room.jpg" time="1500"]

;メニューボタン
@showmenubutton

[chara_mod name="akane"  face="happy"]
[chara_mod name="hi"  face="normal"]
[chara_show name="akane" left=980 top=50]
[chara_show name="hi" left=180 top=50]
[playbgm storage="dozikkomarch.ogg"]

[wait time=1000]

[anim name=akane left="-=580" time=500 effect="easeInSine"]
[playse storage="人にぶつかりました.ogg"]
[wait time=1000]

@layopt layer=message0 visible=true

[chara_mod name="hi"  face="doki"]
#まどか 
ほむらちゃーん！[l]

[cm]

;[chara_mod name="hi"  face="doki"]
#ほむら 
まどか！？[l]

[cm]

[chara_mod name="hi"  face="okoraretai"]
#ほむら  
急に抱き着かないでよ！[l]

[cm]

[chara_mod name="akane"  face="normal"]
#まどか
えー！[r]
いいじゃん[l]

[cm]

#まどか
ところで、[r]
ほむらちゃんは何やってるの？[l]

[cm]

[chara_mod name="hi"  face="nicejitome"]
#ほむら 
はぁ……、[r]
珍しい鳥がいたから見てたのよ[l]

[cm]

[chara_mod name="akane"  face="doki"]
#まどか
え！[r]
どこどこ！？[l]

[cm]

[chara_mod name="akane"  face="happy"]
#まどか
あ！[r]
そういえば、これから新しく出来たカフェ行くんだけど、[r]
一緒に行かない？[l]

[cm]

[chara_mod name="hi"  face="komarimax"]
#ほむら
もう少し鳥を見たいから[r]
遠慮しとくわ[l]

[cm]

[chara_mod name="akane"  face="doki"]
#まどか
えー！[r]
行こうよー[l]

[cm]

@layopt layer=message0 visible=false

[anim name=akane top="+=75" time=50 effect="easeInSine"]
[playse storage="jump1.ogg"]
[wait time=200]

[anim name=akane top="-=75" time=50 effect="easeInSine"]
[wait time=500]

[anim name=akane top="+=75" time=50 effect="easeInSine"]
[playse storage="jump1.ogg"]
[wait time=200]

[anim name=akane top="-=75" time=50 effect="easeInSine"]
[wait time=500]

[wa]

@layopt layer=message0 visible=true

[chara_mod name="hi"  face="okoraretai"]
#ほむら
ちょっと人前で[r]
恥ずかしいから辞めなさい！[l]

[cm]

#ほむら
と言うより……[r]
いつまで抱き着いてるのよ！[l]
[mask]
[chara_hide_all]
[chara_hide_all layer=1]
[freeimage layer="base"]
#
@layopt layer=message0 visible=false
[mask_off]
[jump first.ks]
 