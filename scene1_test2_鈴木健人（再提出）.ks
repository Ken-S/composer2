*start
;スクリプトコンポーザ級再試験修正点（01/25）
;シネマスコープは維持したまま、画面に表情のみを映すためカメラのズームを行いました。
;それに伴って、黒画像とキャラクターの表示位置・アニメーションする値を調整しました。


[cm]
[clearfix]
[chara_config pos_mode=false]

[mask time=0]


[position layer="message0" left=20 top=450 width=920 height=150 page=fore visible=true]

[position layer=message0 page=fore margint="35" marginl="50" marginr="70" marginb="60"]

[ptext name="chara_name_area" layer="message0" color="white" size=22 x=50 y=460]

[chara_config ptext="chara_name_area"]

@showmenubutton

;あかね
[chara_new  name="akane" storage="chara/akane/normal.png" jname="あかね"]

;あおい
[chara_new  name="aoi" storage="chara/aoi/normal.png" jname="あおい"]

;あかね表情
[chara_face name="akane" face="normal" storage="chara/akane/normal.png"]
[chara_face name="akane" face="angry" storage="chara/akane/angry.png"]
[chara_face name="akane" face="doki" storage="chara/akane/doki.png"]
[chara_face name="akane" face="happy" storage="chara/akane/happy.png"]
[chara_face name="akane" face="sad" storage="chara/akane/sad.png"]
[chara_face name="akane" face="back" storage="chara/akane/back.png"]


;あおい表情

[chara_face name="aoi" face="normal" storage="chara/aoi/normal.png"]
[chara_face name="aoi" face="angry" storage="chara/aoi/angry.png"]
[chara_face name="aoi" face="doki" storage="chara/aoi/doki.png"]
[chara_face name="aoi" face="happy" storage="chara/aoi/happy.png"]
[chara_face name="aoi" face="sad" storage="chara/aoi/sad.png"]

;背景：水族館

@layopt layer=message0 visible=false


[image layer=0 folder="bgimage" name="suizokukan" storage="suizokukan.jpg" visible=true top=0 left=0 width=1200 height=675]

[image layer="1" folder="bgimage" name="kuro" storage="kuro.jpg" visible="true" top="-785" left="0" time="0" ]
[image layer="1" folder="bgimage" name="kuro" storage="kuro.jpg" visible="true" top="645" left="0" time="0" ]
[camera y=-250 zoom=1.7 time=0 wait=true]

[mask_off time=1000]


[chara_mod name="akane" face="sad"]

[chara_show name="akane" left=280 top=400]

[anim name="akane" left="-=15" time=200]
[anim name="akane" left="+=30" time=200]
[anim name="akane" left="-=15" time=200]
[wa]
@layopt layer=message0 visible=true

#あかね
あおいちゃん、海まで来て[r]
制服って、ないと思うんだ……[p]

[chara_hide name="akane"]

[chara_mod name="aoi" face="angry"]

[chara_show name="aoi" left=280 top=400 wait=false]


#あおい
なに言ってるの？[r]
学生はどんな時でも制服を着用するものよ！[p]

[chara_mod name="aoi" face="happy" wait=false]
[anim name="aoi" left="+=20" time=200]

#あおい
あかねも制服に着替えましょう！[p]


[chara_hide name="aoi"]

[chara_mod name="akane" face="angry" wait=false]

[chara_show name="akane" left=280 top=400]

[anim name="akane" left="+=10" time=400 wait=false]

#あかね
やだよ……、私は海で泳いてくる！[p]

[chara_mod name="akane" face="happy"]

#あかね
じゃあね、あおいちゃん！[p]

@layopt layer=message0 visible=false
[anim name=akane left="+=15" time=200 opacity=255]
[wait time=200]
[chara_hide name="akane" time=300 opacity=0]
[wait time=300]

[mask time=1000]
[freeimage layer=0 time=0]
[freeimage layer=1 time=0]
[reset_camera time=0]
[chara_hide_all time=0]
;背景：リビング
[image layer=0 folder="bgimage" name="living" storage="living.jpg" visible=true top=0 left=0 width=1200 height=675]
[chara_mod name="aoi" face="angry"]
[chara_show name="aoi" left=300 top=50]

[image layer=1 folder="bgimage" name="kuro" storage="kuro.jpg" visible=true top=0 left=0 width=1200 height=675]
@layopt layer=message0 visible=false

@layopt layer=1 opacity=100

[mask_off time=1200]
@layopt layer=message0 visible=true
#あおい
[font size=30]あかね、起きて！[r]
こんなところで寝たら風邪ひくよ！[p]
[font size=22]

@layopt layer=message0 visible=false

[mask time=900]
@layopt layer=1 opacity=50
[mask_off time=900]

[mask time=500]
[freeimage layer=1 time=0]
[mask_off time=500]


[chara_hide name="aoi"]
[chara_mod name="akane" face="sad"]

[chara_show name="akane" left=300 top=100]

[anim name="akane" top="-=50" time=800 effect="easeInOutSine"]
[wa]
[anim name="akane" left="-=10" time=100 effect="easeInOutSine"]

[anim name="akane" left="+=10" time=100 effect="easeInOutSine"]

[anim name="akane" left="-=10" time=100 effect="easeInOutSine"]

[anim name="akane" left="+=10" time=100 effect="easeInOutSine"]


@layopt layer=message0 visible=true


#あかね
……うぅ……寒い……[r]
――っくしゅん！[p]

[chara_hide name="akane"]


@layopt layer=message0 visible=true


[chara_mod name="akane" face="sad"]
[chara_mod name="aoi" face="sad"]


[chara_show name="akane" left=500 top=50 wait=false]

[chara_show name="aoi" left=50 top=50 wait=false]


#あおい
はぁ……、風邪ひいたんじゃない？[p]

[chara_mod name="aoi" face="normal"]


#あおい
ちょっとこっちに来て、おでこ出してみて[p]


[chara_mod name="akane" face="doki"]


#あかね
え？[r]
……あれ……私の海は？[p]


[chara_mod name="aoi" face="angry"]

[anim name="akane" top="-=20" time=100]

[anim name="akane" top="+=20" time=100]

#あおい
なに訳のわからないこと言ってるの！[r]
いいからおでこを出す！[p]

[chara_mod name="akane" face="normal"]

[anim name="aoi" left="+=50" time=150 wait=false]

#あかね
あ……[p]

[chara_mod name="akane" face="happy"]

#あかね
……えっと、それは恥ずかしいよ～[p]

[chara_mod name="akane" face="doki" wait=false]
[anim name="aoi" left="+=200" time=400 wait=false]
[anim name="akane" left="+=30" time=400 wait=true]

#あおい
いいから！[r]
うーん、ちょっと熱っぽい気もするけど……[p]

@layopt layer=message0 visible=false

;ズーム
[camera x=140 y=50 zoom=1.7 time=500]

@layopt layer=message0 visible=true

#あおい
もう少ししっかりおでこかしなさいよ[p]

[anim name="aoi" left="+=40" time=500 wait=false]

[anim name="akane" left="-=10" time=500 wait=false]

[wa]
[wait time=800]

[chara_mod name="aoi" face="sad"]

[anim name="aoi" left="-=40" time=80 wait=false]

#あおい
うーん、やっぱり少しありそうな感じね……[p]

@layopt layer=message0 visible=false

[chara_hide_all]

[camera x=0 y=0 zoom=1 time=800]



;ズーム解除


[chara_mod name="akane" face="angry"]
[chara_show name="akane" left=300 top=50]
@layopt layer=message0 visible=true



#あかね
だ、大丈夫だから！[r]
そろそろ、お風呂入って寝るから！[p]

[chara_mod name="akane" face="happy"]

#あかね
あおいちゃんも風邪ひかないように気を付けてね！[p]

@layopt layer=message0 visible=false


[anim name=akane left="+=30" time=200 opacity=255]
[wait time=200]
[chara_hide name="akane" time=300 opacity=0]
[wait time=300]


[mask time=500]

@layopt layer=message0 visible=false
@hidemenubutton
[chara_hide_all]
[freeimage layer="base"]
[jump first.ks]

